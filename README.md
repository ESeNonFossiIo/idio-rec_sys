# Idio - rec_sys

[Kaggle link](https://www.kaggle.com/rounakbanik/the-movies-dataset)

## setup:
- *make setup* will install all requirements for the python scripts.
- *make nltk-setup* will download all nltk corpus requirements

## code:
- *Idio Coding Challenge - ​Rec Sys.ipynb* : this jupyter notebook contains all the code. In particular it contains:
  - data cleaning and pre-processing:
    - basic  managments of variables like *spoken_languages* or *genres*
    - feeding of *w2v* algoritm with senteces contained in *overview*
    - *w2v* + *k-means* for dimensinal reduction and grouping keywords into clusters
  - comparison between different machine learning algoritms
    - for every algoritm it is reported:
      - *elapsed time* = the time spent for training
      - *accuracy* = the ratio of cases in which the prediction is at most *RATE_TOL* different in term of absolute distance from the test value
      - *good advices* = the ratio of cases in which the prediction and test are both over 6.0 or both below 6.0.
  - the most significant time and/or memory consuming steps are measured using jupiter tags like *%time* and *%memit*.

## possibile improvements:

- study the data correlation and impact of each column to the final risalt. This could help reducing the dimension of the problem and increasing the accuracy
- test different machine learning algoritms and combine them together with algoritms like *VotingClassifier* of *sklearn* or test a deep neural network approach.

## possible errors and how to fix them:

- Encoding errors: remove *.DS_Store* form *data/corpus*.
