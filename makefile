requirements:
	pipreqs --force ./data/

setup:
	pip install -r ./data/requirements.txt

nltk-setup:
	python3 ./data/nltk_requirements.py
